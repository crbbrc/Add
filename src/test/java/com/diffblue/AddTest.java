package com.diffblue;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 * Unit test for simple Add.
 */
public class AddTest
{

   Add a;

    @Before
    public void setup(){
       a = new Add();
    }

    @Test
    public void testAddNoNumber() {
        assertEquals(a.sum(), 0);
    }

    @Test
    public void testAddSingleNumber() {
        assertEquals(a.sum(1), 1);
    }

    @Test
    public void testAddMultipleNumbers()
    {
        assertEquals( a.sum(1,2,3,4), 10);
    }


    @Test
    public void testAddNegativeMultipleNumbers()
    {
        assertEquals( a.sum(-1,2,-3,4), 2);
    }
}
